import { Negociacao } from './index';
export class Negociacoes {
  private _negociacoes: Array<Negociacao> = [];

  adiciona(negociacao: Negociacao): void {
    this._negociacoes.push(negociacao);
  }

  paraArray(): Negociacao[] { // Esse formato é o mesmo que Array<Negociacao>
    return ([] as Negociacao[]).concat(this._negociacoes);
  }
}